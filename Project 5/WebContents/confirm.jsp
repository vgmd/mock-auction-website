<%@ page import="edu.ucla.cs.cs144.*" %>

<html>
<head>
    
</head>
<body>
    <table>
		<tr><td>Item ID:</td><td><%= request.getAttribute("itemId") %></td></tr>
		<tr><td>Item Name:</td><td><%= request.getAttribute("itemName") %></td></tr>
		<tr><td>Buy Price:</td><td><%= request.getAttribute("buyPrice") %></td></tr>
		<tr><td>Credit Card:</td><td><%= request.getAttribute("ccNumber") %></td></tr>
		<tr><td>Time:</td><td><%= request.getAttribute("time") %></td></tr>
	</table>
</body>
</html>