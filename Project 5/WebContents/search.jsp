<%@ page import="edu.ucla.cs.cs144.*" %>

<html>
<head>
    <title>Search Results: <%= request.getAttribute("q") %></title>
<style>
	div.suggestions {
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		border: 1px solid black;
		position: absolute;
		background-color: white;
	}
	div.suggestions div {
		cursor: default;
		padding: 0px 3px;
	}
	div.suggestions div.current {
		background-color: #3366cc;
		color: white;
	}
</style>
<script type='text/javascript' src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="suggestions2.js"></script>
<script type="text/javascript" src="autosuggest2.js"></script>
<script type="text/javascript">
	window.onload = function() {
		var oTextbox = new AutoSuggestControl(document.getElementById("queryBox"), new GoogleSuggestions());
	}
</script>
</head>
<body>
    <!-- For easy searching again -->
    <form name="search" action="./search" method="GET">
        q: <label><input type="text" name="q" id="queryBox"></label>
        numResultsToSkip: <label><input type="text" name="numResultsToSkip" value="0"></label>
        numResultsToReturn: <label><input type="text" name="numResultsToReturn" value="100"></label>
    <input type="Submit" value="Submit">
    </form>
    <hr />
    
    <h1>Search results for: <%= request.getAttribute("q") %>, results <%= request.getAttribute("resultRange") %></h1>
    <hr />
    <table border="1">
        <tr>
            <td>Item ID</td>
            <td>Item Name</td>
        </tr>
        
        <%= request.getAttribute("itemSubtable") %>
    </table>
    <br />
    <%= request.getAttribute("pnlinks") %>
</body>
</html>