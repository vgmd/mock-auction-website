
/**
 * Provides suggestions for state names (USA).
 * @class
 * @scope public
 */
function GoogleSuggestions() {}

/**
 * Request suggestions for the given autosuggest control. 
 * @scope protected
 * @param oAutoSuggestControl The autosuggest control to provide suggestions for.
 */
GoogleSuggestions.prototype.requestSuggestions = function (oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    var inputText = $("#queryBox").val();

	inputText = encodeURI(inputText);
	
    // Start AJAX call
    $.ajax({
        url: '/eBay/suggest',
        type: 'GET',
        data: {
            q : inputText
        }
    }).done(function(data) {
        var aReturnedSuggestions = data.split("\n");
		var aSuggestions = [];
		var sTextboxValue = oAutoSuggestControl.textbox.value;

		if (sTextboxValue.length > 0){
			//search for matching states
			for (var i=0; i < aReturnedSuggestions.length; i++) { 
				if (aReturnedSuggestions[i].indexOf(sTextboxValue) == 0) {
					aSuggestions.push(aReturnedSuggestions[i]);
				} 
			}
		}

        //provide suggestions to the control
        oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
    }).fail(function() {
        //alert("Failed to get suggestions.");
    })
};