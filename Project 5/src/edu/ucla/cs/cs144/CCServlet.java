package edu.ucla.cs.cs144;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.*;
import java.util.*;

public class CCServlet extends HttpServlet implements Servlet {
       
    public CCServlet() {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {       
        try {
            HttpSession session = request.getSession();
			if (session == null) {
				request.getRequestDispatcher("/search.jsp").forward(request, response);
			}
			
			request.setAttribute("itemId", (String) session.getAttribute("itemId"));
			request.setAttribute("itemName", (String) session.getAttribute("itemName"));
			request.setAttribute("buyPrice", (String) session.getAttribute("buyPrice"));
			
			StringBuilder sb = new StringBuilder("https://");
			sb.append(request.getServerName());
			sb.append(":");
			sb.append("8443");
			sb.append(request.getContextPath());
			sb.append("/confirm");
			
			request.setAttribute("confirmURL", sb.toString());
			
            request.getRequestDispatcher("/ccinput.jsp").forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
