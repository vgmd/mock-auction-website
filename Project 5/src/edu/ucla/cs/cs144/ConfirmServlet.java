package edu.ucla.cs.cs144;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.*;
import java.util.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ConfirmServlet extends HttpServlet implements Servlet {
       
    public ConfirmServlet() {}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {       
        try {
			String cc = request.getParameter("cc_number");
            HttpSession session = request.getSession();
			if (session == null) {
				request.getRequestDispatcher("/search.jsp").forward(request, response);
			}
			
			request.setAttribute("itemId", (String) session.getAttribute("itemId"));
			request.setAttribute("itemName", (String) session.getAttribute("itemName"));
			request.setAttribute("buyPrice", (String) session.getAttribute("buyPrice"));
			request.setAttribute("ccNumber", cc);
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date date = new Date();
			request.setAttribute("time", dateFormat.format(date).toString());
			
            request.getRequestDispatcher("/confirm.jsp").forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
