package edu.ucla.cs.cs144;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.xml.parsers.*;
import org.xml.sax.InputSource;
import org.w3c.dom.*;
import java.io.*;
import java.util.*;

public class ItemServlet extends HttpServlet implements Servlet {
       
    public ItemServlet() {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String xml = AuctionSearchClient.getXMLDataForItemId(
                request.getParameter("id")
        );
		
		if (xml == "") {
			request.getRequestDispatcher("/noitem.jsp").forward(request, response);
			return;
		}
        
        try {
            DocumentBuilderFactory dbf =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            
            // Start parsing
            NodeList nodes = doc.getElementsByTagName("Item");
            Element itemElement = (Element) nodes.item(0);
            
            // NOTICE:
            // Some HTML is generated here to reduce the amount of parameters
            // passed into the jsp file.
            
            // Item Details
            String itemDetails = "";
            // Item ID
            itemDetails += "Item ID: " + itemElement.getAttribute("ItemID") + "<hr>";
            // Name
            itemDetails += "Name: " + getElementTextByTagNameNR(itemElement, "Name") + "<hr>";
            // Categories
            itemDetails += "Categories: ";
            Element[] cats = getElementsByTagNameNR(itemElement, "Category");
            for(int j = 0; j < cats.length; j++) 
                itemDetails += getElementText(cats[j]) + (j != cats.length -1 ? ", " : "<hr>");
            // Currently
            itemDetails += "Currently: " + getElementTextByTagNameNR(itemElement, "Currently") + "<hr>";
            // Buy Price
            itemDetails += "Buy Price: " + getElementTextByTagNameNR(itemElement, "Buy_Price") + "<hr>";
            // First Bid
            itemDetails += "First Bid: " + getElementTextByTagNameNR(itemElement, "First_Bid") + "<hr>";
            // Number of Bids
            itemDetails += "Number of Bids: " + getElementTextByTagNameNR(itemElement, "Number_of_Bids") + "<hr>";
            // Location
            Element locationElement = getElementByTagNameNR(itemElement,
                "Location");
            itemDetails += "Location: " + getElementText(locationElement) + "<hr>";
            // Latitude
            itemDetails += "Latitude: " + locationElement.getAttribute("Latitude") + "<hr>";
            // Longitude
            itemDetails += "Longitude: " + locationElement.getAttribute("Longitude") + "<hr>";
            // Country
            itemDetails += "Country: " + getElementTextByTagNameNR(itemElement, "Country") + "<hr>";
            // Started
            itemDetails += "Started: " + getElementTextByTagNameNR(itemElement, "Started") + "<hr>";
            // Ends
            itemDetails += "Ends: " + getElementTextByTagNameNR(itemElement, "Ends") + "<hr>";
            // Description
            itemDetails += "Description: " + getElementTextByTagNameNR(itemElement, "Description") + "<hr><hr>";
            
            // Seller
            String sellerDetails = "";
            Element sellerElement = getElementByTagNameNR(itemElement,
                "Seller");
            sellerDetails += "Seller UserId: " + sellerElement.getAttributes()
                .getNamedItem("UserID").getNodeValue() + "<hr>";
            sellerDetails += "Seller Rating: " + sellerElement.getAttributes()
                .getNamedItem("Rating").getNodeValue() + "<hr><hr>";
            
            //Bids
            String bidDetails = "";
            Element bidsElement = getElementByTagNameNR(itemElement,
                "Bids");
            Element[] bids = getElementsByTagNameNR(bidsElement, "Bid");
            for(int j = bids.length -1; j >= 0; j--) {
                bidDetails += "<tr><td>";
                Element bidderElement = getElementByTagNameNR(bids[j],
                    "Bidder");
                bidDetails += "Seller UserId: " + bidderElement.getAttribute("UserID") + "<hr>";
                bidDetails += "Seller Rating: " + bidderElement.getAttribute("Rating") + "<hr>";
                bidDetails += "Seller Location: " + getElementTextByTagNameNR(bidderElement,
                    "Location") + "<hr>";
                bidDetails += "Seller Country: " + getElementTextByTagNameNR(bidderElement,
                    "Country") + "<hr>";
                
                bidDetails += "Bid Time: " + getElementTextByTagNameNR(bids[j],
                    "Time") + "<hr>";
                    
                bidDetails += "Bid Amount: " + getElementTextByTagNameNR(bids[j],
                    "Amount") + "</td></tr>";
            }
			
			HttpSession session = request.getSession(true);
			session.setAttribute("itemId", itemElement.getAttribute("ItemID"));
			session.setAttribute("itemName", getElementTextByTagNameNR(itemElement, "Name"));
			session.setAttribute("buyPrice", getElementTextByTagNameNR(itemElement, "Buy_Price"));
            
            request.setAttribute("itemDetails", itemDetails);
            request.setAttribute("sellerDetails", sellerDetails);
            request.setAttribute("bidDetails", bidDetails);
            request.setAttribute("itemId", request.getParameter("id"));
			request.setAttribute("latitude", locationElement.getAttribute("Latitude") == "" ? 9999999 : locationElement.getAttribute("Latitude"));
			request.setAttribute("longitude", locationElement.getAttribute("Longitude") == "" ? 9999999 : locationElement.getAttribute("Longitude"));
			request.setAttribute("location", getElementText(locationElement));
			String buttonText = getElementTextByTagNameNR(itemElement, "Buy_Price") == "" ? "" : "<input type=\"Submit\" value=\"Pay Now!\">";
			request.setAttribute("buyPrice", buttonText);
            request.getRequestDispatcher("/item.jsp").forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    // Auxiliary functions pulled from Project 2: MyParser.java
    /* Non-recursive (NR) version of Node.getElementsByTagName(...)
     */
    static Element[] getElementsByTagNameNR(Element e, String tagName) {
        Vector< Element > elements = new Vector< Element >();
        Node child = e.getFirstChild();
        while (child != null) {
            if (child instanceof Element && child.getNodeName().equals(tagName))
            {
                elements.add( (Element)child );
            }
            child = child.getNextSibling();
        }
        Element[] result = new Element[elements.size()];
        elements.copyInto(result);
        return result;
    }
    
    /* Returns the first subelement of e matching the given tagName, or
     * null if one does not exist. NR means Non-Recursive.
     */
    static Element getElementByTagNameNR(Element e, String tagName) {
        Node child = e.getFirstChild();
        while (child != null) {
            if (child instanceof Element && child.getNodeName().equals(tagName))
                return (Element) child;
            child = child.getNextSibling();
        }
        return null;
    }
    
    /* Returns the text associated with the given element (which must have
     * type #PCDATA) as child, or "" if it contains no text.
     */
    static String getElementText(Element e) {
        if (e.getChildNodes().getLength() == 1) {
            Text elementText = (Text) e.getFirstChild();
            return elementText.getNodeValue();
        }
        else
            return "";
    }
    
    /* Returns the text (#PCDATA) associated with the first subelement X
     * of e with the given tagName. If no such X exists or X contains no
     * text, "" is returned. NR means Non-Recursive.
     */
    static String getElementTextByTagNameNR(Element e, String tagName) {
        Element elem = getElementByTagNameNR(e, tagName);
        if (elem != null)
            return getElementText(elem);
        else
            return "";
    }
}
