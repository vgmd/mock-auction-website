package edu.ucla.cs.cs144;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SearchServlet extends HttpServlet implements Servlet {
       
    public SearchServlet() {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        SearchResult[] items = AuctionSearchClient.basicSearch(
                request.getParameter("q"),
                Integer.parseInt(request.getParameter("numResultsToSkip")),
                Integer.parseInt(request.getParameter("numResultsToReturn"))
        );
        
        String pageString = request.getParameter("page");
        int page = (pageString == null) ? 0 : Integer.parseInt(pageString);
        
        String subtable = "";
        int i;
        for(i = page * 20; i < items.length && i < (page + 1) * 20; i++) {
            subtable += "<tr>";
            subtable += "<td><a href='/eBay/item?id=" + items[i].getItemId() +
                "'>" + items[i].getItemId() + "</a></td>";
            subtable += "<td>" + items[i].getName() + "</td>";
            subtable += "</tr>\n";
        }
        
        String next = i >= items.length ? "" :
            "<a href='/eBay/search?q=" + request.getParameter("q") +
            "&numResultsToSkip=" + request.getParameter("numResultsToSkip") +
            "&numResultsToReturn=" + request.getParameter("numResultsToReturn") +
            "&page=" + Integer.toString(page + 1) + "'>Next</a>";
        String previous = page == 0 ? "" :
            "<a href='/eBay/search?q=" + request.getParameter("q") +
            "&numResultsToSkip=" + request.getParameter("numResultsToSkip") +
            "&numResultsToReturn=" + request.getParameter("numResultsToReturn") +
            "&page=" + Integer.toString(page - 1) + "'>Previous</a>";
        
        request.setAttribute("resultRange", Integer.toString(page * 20) + "-" +
            Integer.toString(Math.min(items.length, (page + 1) * 20)));
        request.setAttribute("itemSubtable", subtable);
        request.setAttribute("q", request.getParameter("q"));
        request.setAttribute("pnlinks", previous + " " + next);
        request.getRequestDispatcher("/search.jsp").forward(request, response);
    }
}
