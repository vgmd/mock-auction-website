CREATE TABLE Items(
	itemId int,
	name VARCHAR(255),
	currently DECIMAL(8,2) NOT NULL,
	buyPrice DECIMAL(8,2),
	firstBid DECIMAL(8,2) NOT NULL,
	numBids int DEFAULT 0,
	location VARCHAR(255) NOT NULL,
	latitude DECIMAL(10,8),
	longitude DECIMAL(11,8),
	country VARCHAR(255) NOT NULL,
	started TIMESTAMP NOT NULL,
	ends TIMESTAMP NOT NULL,
	seller VARCHAR(255),
	description VARCHAR(4000)
) ENGINE=InnoDB;

CREATE TABLE Sellers(
	userId VARCHAR(255),
	rating int DEFAULT 0
) ENGINE=InnoDB;

CREATE TABLE Bidders(
	userId VARCHAR(255),
	rating int DEFAULT 0,
	location VARCHAR(255),
	country VARCHAR(255)
) ENGINE=InnoDB;

CREATE TABLE Bids(
	itemId int,
	bidder VARCHAR(255),
	time TIMESTAMP,
	amount DECIMAL(8,2) DEFAULT 0
) ENGINE=InnoDB;

CREATE TABLE Categories(
	itemId int,
	category VARCHAR(255)
) ENGINE=InnoDB;

ALTER TABLE Items ADD PRIMARY KEY (itemId);
ALTER TABLE Sellers ADD PRIMARY KEY (userId);
ALTER TABLE Bidders ADD PRIMARY KEY (userId);
ALTER TABLE Items ADD FOREIGN KEY (seller) REFERENCES Sellers(userId);
ALTER TABLE Bids ADD FOREIGN KEY (itemId) REFERENCES Items(itemId);
ALTER TABLE Bids ADD FOREIGN KEY (bidder) REFERENCES Bidders(userId);
ALTER TABLE Categories ADD FOREIGN KEY (itemId) REFERENCES Items(itemId);