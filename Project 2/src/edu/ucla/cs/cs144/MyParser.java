/* CS144
 *
 * Parser skeleton for processing item-???.xml files. Must be compiled in
 * JDK 1.5 or above.
 *
 * Instructions:
 *
 * This program processes all files passed on the command line (to parse
 * an entire diectory, type "java MyParser myFiles/*.xml" at the shell).
 *
 * At the point noted below, an individual XML file has been parsed into a
 * DOM Document node. You should fill in code to process the node. Java's
 * interface for the Document Object Model (DOM) is in package
 * org.w3c.dom. The documentation is available online at
 *
 * http://java.sun.com/j2se/1.5.0/docs/api/index.html
 *
 * A tutorial of Java's XML Parsing can be found at:
 *
 * http://java.sun.com/webservices/jaxp/
 *
 * Some auxiliary methods have been written for you. You may find them
 * useful.
 */

package edu.ucla.cs.cs144;

import java.io.*;
import java.text.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ErrorHandler;


class MyParser {
    
    static final String columnSeparator = "|*|";
    static DocumentBuilder builder;
    
    static final String[] typeName = {
	"none",
	"Element",
	"Attr",
	"Text",
	"CDATA",
	"EntityRef",
	"Entity",
	"ProcInstr",
	"Comment",
	"Document",
	"DocType",
	"DocFragment",
	"Notation",
    };
    
    static Set<Item> itemSet = new HashSet<Item>();
    static Set<Seller> sellerSet = new HashSet<Seller>();
    static Set<Bidder> bidderSet = new HashSet<Bidder>();
    static Set<Bid> bidSet = new HashSet<Bid>();
	static Set<Category> categorySet = new HashSet<Category>();
    
    static class MyErrorHandler implements ErrorHandler {
        
        public void warning(SAXParseException exception)
        throws SAXException {
            fatalError(exception);
        }
        
        public void error(SAXParseException exception)
        throws SAXException {
            fatalError(exception);
        }
        
        public void fatalError(SAXParseException exception)
        throws SAXException {
            exception.printStackTrace();
            System.out.println("There should be no errors " +
                               "in the supplied XML files.");
            System.exit(3);
        }
        
    }
	
	static class Item {
		public String itemID;
		public String name;
		public String currently;
		public String buyPrice;
		public String firstBid;
		public String numBids;
		public String location;
		public String latitude;
		public String longitude;
		public String country;
		public String started;
		public String ends;
		public String seller;
		public String description;
        
        public Item(String id, String n, String cur,
                    String buy, String first, String num, String loc,
                    String lat, String lon, String coun, String start,
                    String end, String sell, String desc) {
                        itemID = id;
                        name = n;
                        currently = cur;
                        buyPrice = buy;
                        firstBid = first;
                        numBids = num;
                        location = loc;
                        latitude = lat;
                        longitude = lon;
                        country = coun;
                        started = start;
                        ends = end;
                        seller = sell;
                        description = desc;
        }
	}
	
	static class Seller {
		public String userId;
		public String rating;
        
        public Seller(String id, String rate) {
            userId = id;
            rating = rate;
        }
	}
    
	static class Bidder {
		public String userId;
		public String rating;
		public String location;
		public String country;
        
        public Bidder(String id, String rate, String loc, String coun) {
            userId = id;
            rating = rate;
            location = loc;
            country = coun;
        }
	}
    
    static class Bid {
        public String itemID;
        public String bidderId;
        public String time;
        public String amount;
        
        public Bid(String iId, String bId, String t, String a) {
            itemID = iId;
            bidderId = bId;
            time = t;
            amount = a;
        }
    }
	
	static class Category {
		public String itemID;
		public String category;
		
		public Category(String iId, String cat) {
			itemID = iId;
			category = cat;
		}
	}
    
    static String sqlDate(String d) {
        String ts = "";
        if(d.equals(ts)) return d;
        String pattern = "MMM-dd-yy HH:mm:ss";
        String newPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat format = new SimpleDateFormat(pattern);

        try {
            Date date = format.parse(d);
            ts = new SimpleDateFormat(newPattern).format(date);
        } catch(ParseException e) {
            e.printStackTrace();
        }
        return ts;
    }
    
    static String escapeCsv(String s) {
        String unquoted = s.replace("\"", "\"\"");
        String unslashed = unquoted.replace("\\", "\\\\");
        return unslashed;
    }
    
    static void writeCSV() {
        try {
            // Item CSV
            FileWriter itemWriter = new FileWriter("items.csv");
            Iterator itemIterator = itemSet.iterator();
            while(itemIterator.hasNext()) {
                Item item = (Item) itemIterator.next();
                String itemLine = "";
                
                itemLine += item.itemID + ",";
                itemLine += "\"" + escapeCsv(item.name) + "\",";
                itemLine += "\"" + item.currently + "\",";
                itemLine += "\"" + item.buyPrice + "\",";
                itemLine += "\"" + item.firstBid + "\",";
                itemLine += item.numBids + ",";
                itemLine += "\"" + escapeCsv(item.location) + "\",";
                itemLine += "\"" + item.latitude + "\",";
                itemLine += "\"" + item.longitude + "\",";
                itemLine += "\"" + item.country + "\",";
                itemLine += "\"" + sqlDate(item.started) + "\",";
                itemLine += "\"" + sqlDate(item.ends) + "\",";
                itemLine += "\"" + item.seller + "\",";
                itemLine += "\"" + escapeCsv(item.description) + "\"\n";
                itemWriter.append(itemLine);
            }
            itemWriter.flush();
            itemWriter.close();
            
            // Seller CSV
            FileWriter sellerWriter = new FileWriter("sellers.csv");
            Iterator sellerIterator = sellerSet.iterator();
            while(sellerIterator.hasNext()) {
                Seller seller = (Seller) sellerIterator.next();
                String sellerLine = "";
                
                sellerLine += "\"" + escapeCsv(seller.userId) + "\",";
                sellerLine += seller.rating + "\n";
                sellerWriter.append(sellerLine);
            }
            sellerWriter.flush();
            sellerWriter.close();
            
            // Bidder CSV
            FileWriter bidderWriter = new FileWriter("bidders.csv");
            Iterator bidderIterator = bidderSet.iterator();
            while(bidderIterator.hasNext()) {
                Bidder bidder = (Bidder) bidderIterator.next();
                String bidderLine = "";
                
                bidderLine += "\"" + escapeCsv(bidder.userId) + "\",";
                bidderLine += bidder.rating + ",";
                bidderLine += "\"" + escapeCsv(bidder.location) + "\",";
                bidderLine += bidder.country + "\n";
                bidderWriter.append(bidderLine);
            }
            bidderWriter.flush();
            bidderWriter.close();
            
            // Bidder CSV
            FileWriter bidWriter = new FileWriter("bids.csv");
            Iterator bidIterator = bidSet.iterator();
            while(bidIterator.hasNext()) {
                Bid bid = (Bid) bidIterator.next();
                String bidLine = "";
                
                bidLine += bid.itemID + ",";
                bidLine += "\"" + escapeCsv(bid.bidderId) + "\",";
                bidLine += "\"" + sqlDate(bid.time) + "\",";
                bidLine += "\"" + bid.amount + "\"\n";
                bidWriter.append(bidLine);
            }
            bidWriter.flush();
            bidWriter.close();
			
			// Categories CSV
            FileWriter categoryWriter = new FileWriter("categories.csv");
            Iterator categoryIterator = categorySet.iterator();
            while(categoryIterator.hasNext()) {
                Category category = (Category) categoryIterator.next();
                String categoryLine = "";
                
                categoryLine += category.itemID + ",";
                categoryLine += "\"" + escapeCsv(category.category) + "\"\n";
                categoryWriter.append(categoryLine);
            }
            categoryWriter.flush();
            categoryWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /* Non-recursive (NR) version of Node.getElementsByTagName(...)
     */
    static Element[] getElementsByTagNameNR(Element e, String tagName) {
        Vector< Element > elements = new Vector< Element >();
        Node child = e.getFirstChild();
        while (child != null) {
            if (child instanceof Element && child.getNodeName().equals(tagName))
            {
                elements.add( (Element)child );
            }
            child = child.getNextSibling();
        }
        Element[] result = new Element[elements.size()];
        elements.copyInto(result);
        return result;
    }
    
    /* Returns the first subelement of e matching the given tagName, or
     * null if one does not exist. NR means Non-Recursive.
     */
    static Element getElementByTagNameNR(Element e, String tagName) {
        Node child = e.getFirstChild();
        while (child != null) {
            if (child instanceof Element && child.getNodeName().equals(tagName))
                return (Element) child;
            child = child.getNextSibling();
        }
        return null;
    }
    
    /* Returns the text associated with the given element (which must have
     * type #PCDATA) as child, or "" if it contains no text.
     */
    static String getElementText(Element e) {
        if (e.getChildNodes().getLength() == 1) {
            Text elementText = (Text) e.getFirstChild();
            return elementText.getNodeValue();
        }
        else
            return "";
    }
    
    /* Returns the text (#PCDATA) associated with the first subelement X
     * of e with the given tagName. If no such X exists or X contains no
     * text, "" is returned. NR means Non-Recursive.
     */
    static String getElementTextByTagNameNR(Element e, String tagName) {
        Element elem = getElementByTagNameNR(e, tagName);
        if (elem != null)
            return getElementText(elem);
        else
            return "";
    }
    
    /* Returns the amount (in XXXXX.xx format) denoted by a money-string
     * like $3,453.23. Returns the input if the input is an empty string.
     */
    static String strip(String money) {
        if (money.equals(""))
            return money;
        else {
            double am = 0.0;
            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            try { am = nf.parse(money).doubleValue(); }
            catch (ParseException e) {
                System.out.println("This method should work for all " +
                                   "money values you find in our data.");
                System.exit(20);
            }
            nf.setGroupingUsed(false);
            return nf.format(am).substring(1);
        }
    }
    
    /* Process one items-???.xml file.
     */
    static void processFile(File xmlFile) {
        Document doc = null;
        try {
            doc = builder.parse(xmlFile);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(3);
        }
        catch (SAXException e) {
            System.out.println("Parsing error on file " + xmlFile);
            System.out.println("  (not supposed to happen with supplied XML files)");
            e.printStackTrace();
            System.exit(3);
        }
        
        /* At this point 'doc' contains a DOM representation of an 'Items' XML
         * file. Use doc.getDocumentElement() to get the root Element. */
        System.out.println("Successfully parsed - " + xmlFile);
        
        /* Fill in code here (you will probably need to write auxiliary
            methods). */
        
        NodeList nodes = doc.getElementsByTagName("Items");
        Element itemList = (Element) nodes.item(0);
        Element[] items = getElementsByTagNameNR(itemList, "Item");
        
        for(int i = 0; i < items.length; i++) {
            Element itemElement = items[i];
            
            String itemId = itemElement.getAttribute("ItemID");
            
            String name = getElementTextByTagNameNR(itemElement, "Name");
            
            Element[] cats = getElementsByTagNameNR(itemElement, "Category");
            for(int j = 0; j < cats.length; j++) 
                categorySet.add(new Category(itemId, getElementText(cats[j])));
            String currently = getElementTextByTagNameNR(itemElement,
                "Currently");
            String currentlyValue = strip(currently);

            String buyPrice = getElementTextByTagNameNR(itemElement,
                "Buy_Price");
            String buyPriceValue = strip(buyPrice);
            
            String firstBid = getElementTextByTagNameNR(itemElement,
                "First_Bid");
            String firstBidValue = strip(firstBid);
            
            String numBids = getElementTextByTagNameNR(itemElement,
                "Number_of_Bids");
                
            // Bids
            Element bidsElement = getElementByTagNameNR(itemElement,
                "Bids");
            Element[] bids = getElementsByTagNameNR(bidsElement, "Bid");
            for(int j = 0; j < bids.length; j++) {
                Element bidderElement = getElementByTagNameNR(bids[j],
                    "Bidder");
                String bidderRating = bidderElement.getAttribute("Rating");
                String bidderId = bidderElement.getAttribute("UserID");
                String bidderLocation = getElementTextByTagNameNR(bidderElement,
                    "Location");
                String bidderCountry = getElementTextByTagNameNR(bidderElement,
                    "Country");
                bidderSet.add(new Bidder(bidderId, bidderRating,
                                            bidderLocation, bidderCountry));
                
                String time = getElementTextByTagNameNR(bids[j],
                    "Time");
                    
                String amount = getElementTextByTagNameNR(bids[j],
                    "Amount");
                String amountValue = strip(amount);
                
                bidSet.add(new Bid(itemId, bidderId, time, amountValue));
            }
            
            Element locationElement = getElementByTagNameNR(itemElement,
                "Location");
            String location = getElementText(locationElement);
            String latitude = locationElement.getAttribute("Latitude");
            String longitude = locationElement.getAttribute("Longitude");
            
            String country = getElementTextByTagNameNR(itemElement,
                "Country");
                
            String started = getElementTextByTagNameNR(itemElement,
                "Started");
                
            String ends = getElementTextByTagNameNR(itemElement,
                "Ends");
            
            // Seller
            Element sellerElement = getElementByTagNameNR(itemElement,
                "Seller");
            String sellerRating = sellerElement.getAttributes()
                .getNamedItem("Rating").getNodeValue();
            String sellerId = sellerElement.getAttributes()
                .getNamedItem("UserID").getNodeValue();
            
            sellerSet.add(new Seller(sellerId, sellerRating));
            
            String description = getElementTextByTagNameNR(itemElement,
                "Description");
            String truncatedDescription = description.substring(0,
                Math.min(description.length(), 4000));
            
            itemSet.add(new Item(itemId, name, currentlyValue, buyPriceValue,
                                    firstBidValue, numBids, location, latitude,
                                    longitude, country, started, ends,
                                    sellerId, description));
        }
		
        System.out.println(items.length + " items processed.");
        // Now write to CSV file
        writeCSV();
        
        /**************************************************************/
        
    }
    
    public static void main (String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java MyParser [file] [file] ...");
            System.exit(1);
        }
        
        /* Initialize parser. */
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            factory.setIgnoringElementContentWhitespace(true);      
            builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new MyErrorHandler());
        }
        catch (FactoryConfigurationError e) {
            System.out.println("unable to get a document builder factory");
            System.exit(2);
        } 
        catch (ParserConfigurationException e) {
            System.out.println("parser was unable to be configured");
            System.exit(2);
        }
        
        /* Process all files listed on command line. */
        for (int i = 0; i < args.length; i++) {
            File currentFile = new File(args[i]);
            processFile(currentFile);
        }
    }
}
