-- 1. Number of users in database
SELECT COUNT(*)
FROM (SELECT Sellers.userId from Sellers
      UNION
      SELECT Bidders.userId from Bidders) a;

-- 2. Number of items in "New York"
SELECT COUNT(*)
FROM Items
WHERE location COLLATE latin1_general_cs LIKE "New York";

-- 3. Number of auctions in exactly 4 categories
SELECT COUNT(*)
FROM (
	SELECT itemId, COUNT(DISTINCT category) as numCategories
	FROM Categories
	GROUP BY (itemId)
) a
WHERE a.numCategories = 4;

-- 4. ID(s) of current (unsold) auctions with the highest bid
SELECT itemId
FROM Bids t1
INNER JOIN (
	SELECT MAX(amount) highest
	FROM Bids
	WHERE itemId IN (
		SELECT itemId
		FROM Items
		WHERE ends > '2001-12-20 00:00:01'
	)
) t2
ON t1.amount = t2.highest;
	
-- 5. Number of sellers with rating > 1000
SELECT COUNT(*)
FROM Sellers
WHERE rating > 1000;

-- 6. Number of users who are both sellers and bidders
SELECT COUNT(*)
FROM Sellers
INNER JOIN Bidders
ON Sellers.userId = Bidders.userId;

-- 7. Number categories that have at least 1 item with a bid > $100
SELECT COUNT(DISTINCT category)
FROM (
    SELECT itemId
    FROM Bids
    WHERE Amount > 100
    ) a LEFT JOIN Categories
ON a.itemId = Categories.itemId;