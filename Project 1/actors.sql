-- 1. Create table: Actors(Name:VARCHAR(40), Movie:VARCHAR(80), Year:INTEGER, Role:VARCHAR(40))
CREATE TABLE Actors(
	Name VARCHAR(40),
	Movie VARCHAR(80),
	Year int,
	Role VARCHAR(40)
);

-- 2. Load the actors.csv (~/data/actors.csv) into Actors table
LOAD DATA LOCAL INFILE '~/data/actors.csv' 
INTO TABLE Actors
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

-- 3. Give me the names of all the actors in the movie 'Die Another Day'
SELECT Name FROM Actors WHERE Movie = 'Die Another Day';

-- 4. Drop the Actors table from MySQL
DROP TABLE Actors;
