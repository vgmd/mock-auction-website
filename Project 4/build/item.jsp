<%@ page import="edu.ucla.cs.cs144.*" %>

<html>
<head>
    <title>Search Results: <%= request.getAttribute("itemId") %></title>
<script type="text/javascript" 
    src="http://maps.google.com/maps/api/js?sensor=false"> 
</script> 
<script type="text/javascript">
	var geocoder;
	var map;
	function initialize() {
		geocoder = new google.maps.Geocoder();
		var lat = <%= request.getAttribute("latitude") %>;
		var lon = <%= request.getAttribute("longitude") %>;
		
		if (lat == 9999999 && lon == 9999999) {
			var myOptions = { 
					zoom: 14, // default is 8
					center: new google.maps.LatLng(0, 0),
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
			map = new google.maps.Map(document.getElementById("map_canvas"),
				myOptions);
			codeAddress();
		} else {
			var latlng = new google.maps.LatLng(lat, lon); 
			var myOptions = { 
					zoom: 14, // default is 8  
					center: latlng, 
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
			map = new google.maps.Map(document.getElementById("map_canvas"), 
				myOptions);
		}
	}
	
	function codeAddress() {
		var address = "<%= request.getAttribute("location") %>";
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
			}
		});
}
</script> 
</head>
<body onload="initialize()">
    <!-- For easy searching again -->
    <form name="search" action="./item" method="GET">
        id: <label><input type="text" name="id"></label><br/>
    <input type="Submit" value="Submit">
    </form>
    <hr />
    
    <h1>Search results for: <%= request.getAttribute("itemId") %></h1>
    <hr />

    <h2>Item Details</h2>
    <%= request.getAttribute("itemDetails") %>
    <br>
    
    <h2>Seller Details</h2>
    <%= request.getAttribute("sellerDetails") %>
    <br>
    
    <h2>Bid Details</h2>
    <table border=2>
        <%= request.getAttribute("bidDetails") %>
    </table>
    <br>
	
	<div id="map_canvas" style="width:30%; height:30%"></div>
</body>
</html>