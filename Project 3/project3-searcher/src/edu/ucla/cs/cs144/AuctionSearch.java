package edu.ucla.cs.cs144;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Formatter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import edu.ucla.cs.cs144.DbManager;
import edu.ucla.cs.cs144.SearchRegion;
import edu.ucla.cs.cs144.SearchResult;

public class AuctionSearch implements IAuctionSearch {
	
	class Bidder {
		public String userId;
		public int rating;
		public String location;
		public String country;
	};
	
	class Bid {
		public Bidder bidder;
		public String bidderId;
		public Timestamp time;
		public double amount;
	};

	/* 
         * You will probably have to use JDBC to access MySQL data
         * Lucene IndexSearcher class to lookup Lucene index.
         * Read the corresponding tutorial to learn about how to use these.
         *
	 * You may create helper functions or classes to simplify writing these
	 * methods. Make sure that your helper functions are not public,
         * so that they are not exposed to outside of this class.
         *
         * Any new classes that you create should be part of
         * edu.ucla.cs.cs144 package and their source files should be
         * placed at src/edu/ucla/cs/cs144.
         *
         */
	
	public SearchResult[] basicSearch(String query, int numResultsToSkip, 
			int numResultsToReturn) {
        try {
            // Setup
            IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(
                FSDirectory.open(new File("/var/lib/lucene/index"))));
            QueryParser parser = new QueryParser("totalText", new StandardAnalyzer());
            
            // Parse the query and perform the search
            Query q = parser.parse(query);
            TopDocs topDocs = searcher.search(
                q, numResultsToSkip + numResultsToReturn);
            ScoreDoc[] hits = topDocs.scoreDocs;
            
            // Hash set to remove duplicates
            HashSet resultSet = new HashSet();
            
            // Fill the array, accounting for offset
            for (int i = numResultsToSkip; i < hits.length &&
                    resultSet.size() < numResultsToReturn; i++) {
                Document doc = searcher.doc(hits[i].doc);
                resultSet.add(new SearchResult(doc.get("itemId"), doc.get("name")));
            }
            
            // Create the resulting array of appropriate size
            int size = Math.min(resultSet.size(), numResultsToReturn);
            SearchResult[] results = new SearchResult[size];
            int j = 0;
            
            // Fill the array
            Iterator<SearchResult> resIterator = resultSet.iterator();
            while(resIterator.hasNext()) {
                results[j++] = resIterator.next();
            }
            
            return results;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        return new SearchResult[0];
	}

	public SearchResult[] spatialSearch(String query, SearchRegion region,
			int numResultsToSkip, int numResultsToReturn) {
		
		// TODO: Your code here!
		SearchResult[] basicSearchResult = basicSearch(query, 0, Integer.MAX_VALUE);
		if (basicSearchResult.length == 0)
			return basicSearchResult;
		
		// Create a HashMap from our basic search results. We will use this to intersect with our spatial search results.
		HashMap basicResultMap = new HashMap();
		for (int i = 0; i < basicSearchResult.length; i++) {
			basicResultMap.put(basicSearchResult[i].getItemId(), basicSearchResult[i]);
		}
		
		Connection conn = null;
		
		try {
			conn = DbManager.getConnection(true);
		} catch (SQLException ex) {
			System.out.println(ex);
		}
		
		HashSet resultSet = new HashSet();
		try {
			Statement stmt = conn.createStatement();
			
			// Build SQL Query out of bounding rectangle
			Formatter f = new Formatter();
			f.format("SELECT itemId FROM SpatialIndex WHERE MBRContains(GeomFromText('Polygon((%f %f, %f %f, %f %f, %f %f, %f %f))'), coordinates)",
				region.getLx(), region.getLy(),
				region.getLx(), region.getRy(),
				region.getRx(), region.getRy(),
				region.getRx(), region.getLy(),
				region.getLx(), region.getLy()
			);
						
			ResultSet rs = stmt.executeQuery(f.toString());
			while (rs.next()) {
				String itemId = Integer.toString(rs.getInt("itemId"));
				if (basicResultMap.containsKey(itemId)) {
					resultSet.add(basicResultMap.get(itemId));
				}
			}
			
			// Create the resulting array of appropriate size
            int size = Math.min(resultSet.size(), numResultsToReturn);
            SearchResult[] results = new SearchResult[size];
            int j = 0;
            
            // Fill the array
            Iterator<SearchResult> resIterator = resultSet.iterator();
            while(resIterator.hasNext()) {
                results[j++] = resIterator.next();
            }
            
            return results;
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage()); 
		}
		
		return new SearchResult[0];
	}

	public String getXMLDataForItemId(String itemId) {
		// TODO: Your code here!
		Connection conn = null;
		
		try {
			conn = DbManager.getConnection(true);
		} catch (SQLException ex) {
			System.out.println(ex);
		}
		
		StringBuilder sb = new StringBuilder();
		
		try {
			// Query the Items table
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Items WHERE itemId = " + itemId);
			if (!rs.next())
				return "";
			
			String name = rs.getString("name");
			double currently = rs.getDouble("currently");
			double buyPrice = rs.getDouble("buyPrice");
			double firstBid = rs.getDouble("firstBid");
			int numBids = rs.getInt("numBids");
			String location = rs.getString("location");
			double latitude = rs.getDouble("latitude");
			double longitude = rs.getDouble("longitude");
			String country = rs.getString("country");
			Timestamp started = rs.getTimestamp("started");
			Timestamp ends = rs.getTimestamp("ends");
			String seller = rs.getString("seller");
			String description = rs.getString("description");
			
			// Query the Sellers table
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Sellers WHERE userId = '" + seller + "'");
			int rating = 0;
			if (rs.next())
				rating = rs.getInt("rating");
			
			// Query the Bids table
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Bids WHERE itemId = " + itemId);
			ArrayList<Bid> bids = new ArrayList<Bid>();
			while (rs.next()) {
				Bid bid = new Bid();
				bid.bidderId = rs.getString("bidder");
				bid.time = rs.getTimestamp("time");
				bid.amount = rs.getDouble("amount");
				bids.add(bid);
			}
			
			// Query the Bidders table
			for (int i = 0; i < bids.size(); i++) {
				stmt = conn.createStatement();
				rs = stmt.executeQuery("SELECT * FROM Bidders WHERE userId = '" + bids.get(i).bidderId + "'");
				while (rs.next()) {
					Bidder bidder = new Bidder();
					bidder.userId = rs.getString("userId");
					bidder.rating = rs.getInt("rating");
					bidder.location = rs.getString("location");
					bidder.country = rs.getString("country");
					bids.get(i).bidder = bidder;
				}
			}
			
			// Query the Categories table
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Categories WHERE itemId = " + itemId);
			ArrayList<String> categories = new ArrayList<String>();
			while (rs.next()) {
				categories.add(rs.getString("category"));
			}
			
			// Build the XML
			String TAB = "  ";
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			
			sb.append("<Item ItemID=\"" + itemId + "\">\n");
			sb.append(TAB + "<Name>" + unescape(name, true) + "</Name>\n");
			for (int i = 0; i < categories.size(); i++) {
				sb.append(TAB + "<Category>" + unescape(categories.get(i), true) + "</Category>\n");
			}
			sb.append(TAB + "<Currently>$" + String.format("%.2f", currently) + "</Currently>\n");
			if (buyPrice != 0.0)
				sb.append(TAB + "<Buy_Price>$" + String.format("%.2f", buyPrice) + "</Buy_Price>\n");
			sb.append(TAB + "<First_Bid>$" + String.format("%.2f", firstBid) + "</First_Bid>\n");
			sb.append(TAB + "<Number_of_Bids>" + numBids + "</Number_of_Bids>\n");
			if (numBids == 0) {
				sb.append(TAB + "<Bids />\n");
			} else {
				sb.append(TAB + "<Bids>\n");
				for (int i = 0; i < bids.size(); i++) {
					sb.append(TAB + TAB + "<Bid>\n");
					sb.append(String.format("%s%s%s<Bidder Rating=\"%d\" UserId=\"%s\">\n", TAB, TAB, TAB, bids.get(i).bidder.rating, unescape(bids.get(i).bidder.userId, false)));
					if (bids.get(i).bidder.location != null)
						sb.append(String.format("%s%s%s%s<Location>%s</Location>\n", TAB, TAB, TAB, TAB, unescape(bids.get(i).bidder.location, true)));
					if (bids.get(i).bidder.country != null)
						sb.append(String.format("%s%s%s%s<Country>%s</Country>\n", TAB, TAB, TAB, TAB, unescape(bids.get(i).bidder.country, true)));
					sb.append(TAB + TAB + TAB + "</Bidder>\n");
					Date oldFormat = formatter.parse(bids.get(i).time.toString());
					sb.append(TAB + TAB + TAB + "<Time>" + new SimpleDateFormat("MMM-dd-yy HH:mm:ss").format(oldFormat) + "</Time>\n");
					sb.append(String.format("%s%s%s<Amount>$%.2f</Amount>\n", TAB, TAB, TAB, bids.get(i).amount));
				}
				sb.append(TAB + "</Bids>\n");
			}
			if (latitude == 0.0 && longitude == 0.0)
				sb.append(String.format("%s<Location>%s</Location>\n", TAB, unescape(location, true)));
			else
				sb.append(String.format("%s<Location Latitude=\"%.6f\" Longitude=\"%.6f\">%s</Location>\n", TAB, latitude, longitude, unescape(location, true)));
			sb.append(String.format("%s<Country>%s</Country>\n", TAB, unescape(country, true)));
			Date oldFormat = formatter.parse(started.toString());
			sb.append(String.format("%s<Started>%s</Started>\n", TAB, new SimpleDateFormat("MMM-dd-yy HH:mm:ss").format(oldFormat)));
			oldFormat = formatter.parse(ends.toString());
			sb.append(String.format("%s<Ends>%s</Ends>\n", TAB, new SimpleDateFormat("MMM-dd-yy HH:mm:ss").format(oldFormat)));
			sb.append(String.format("%s<Seller Rating=\"%d\" UserID=\"%s\" />\n", TAB, rating, unescape(seller, false)));
			if (description.equals(""))
				sb.append(TAB + "<Description />\n");
			else
				sb.append(String.format("%s<Description>%s</Description>\n", TAB, unescape(description, true)));
			sb.append("</Item>\n");
			
		} catch (SQLException ex) {
			System.out.println(ex);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			conn.close();
		} catch (SQLException ex) {
			System.out.println(ex);
		}
		
		return sb.toString();
	}
	
	public String echo(String message) {
		return message;
	}
	
	public String unescape(String s, boolean parsed) {
		StringBuffer sb = new StringBuffer(s);
		for (int i = 0; i < sb.length(); i++) {
			switch(sb.charAt(i)) {
				case '"':
					if (parsed)
						sb.replace(i, i+1, "&quot;");
					else
						sb.replace(i, i+1, "\\\"");
					break;
				case '\'':
					sb.replace(i, i+1, "&apos;");
					break;
				case '&':
					sb.replace(i, i+1, "&amp;");
					break;
				case '<':
					sb.replace(i, i+1, "%lt;");
					break;
				case '>':
					sb.replace(i, i+1, "&gt;");
					break;
				case '\\':
					if (!parsed)
						sb.replace(i, i+1, "\\\\");
					break;
			}
		}
		return sb.toString();
	}

}
