package edu.ucla.cs.cs144;

import java.util.Calendar;
import java.util.Date;

import edu.ucla.cs.cs144.AuctionSearch;
import edu.ucla.cs.cs144.SearchRegion;
import edu.ucla.cs.cs144.SearchResult;

public class AuctionSearchTest {
	public static void main(String[] args1)
	{
		AuctionSearch as = new AuctionSearch();

		String message = "Test message";
		String reply = as.echo(message);
		System.out.println("Reply: " + reply);
		
		String query = "superman";
		SearchResult[] basicResults = as.basicSearch(query, 0, 20);
		System.out.println("Basic Seacrh Query: " + query);
		System.out.println("Received " + basicResults.length + " results");
		for(SearchResult result : basicResults) {
			System.out.println(result.getItemId() + ": " + result.getName());
		}
		
		SearchRegion region =
		    new SearchRegion(33.774, -118.63, 34.201, -117.38);
		//SearchRegion region =
		    //new SearchRegion(-999.999, -999.999, 999.999, 999.999);
		SearchResult[] spatialResults = as.spatialSearch("camera", region, 0, 999);
		System.out.println("Spatial Seacrh");
		System.out.println("Received " + spatialResults.length + " results");
		for(SearchResult result : spatialResults) {
			System.out.println(result.getItemId() + ": " + result.getName());
		}
		
		String itemId = "1497595758";
		//String itemId = "1497595357";
		String item = as.getXMLDataForItemId(itemId);
		System.out.println("XML data for ItemId: " + itemId);
		System.out.println(item);

		// Add your own test here
		SearchResult[] superman = as.basicSearch("superman", 0, 99999);
		System.out.println("Received " + superman.length + " results");
		SearchResult[] kitchenware = as.basicSearch("kitchenware", 0, 9999);
		System.out.println("Received " + kitchenware.length + " results");
		SearchResult[] st = as.basicSearch("star trek", 0, 99999);
		System.out.println("Received " + st.length + " results");
        //for(SearchResult result : st) {
		//	System.out.println(result.getItemId() + ": " + result.getName());
		//}
	}
}
