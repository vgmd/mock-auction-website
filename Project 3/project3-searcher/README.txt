UCLA CS144 WINTER QUARTER PROJECT 2

THE MYSTERY MEN

Bryan Lau (404015053)
Greg Mogavero (104036491)

Database Schema
	Items
	+---------------+--------------+-------+
    | FIELD         | TYPE         | ATTR  |
    +---------------+--------------+-------+
    | itemId        | INTEGER      | PRI   |
    | name          | VARCHAR(255) | NTNUL |
    | currently     | DECIMAL(8,2) | NTNUL |
    | buyPrice      | DECIMAL(8,2) |       |
    | firstBid      | DECIMAL(8,2) | NTNUL |
    | numBids       | INTEGER      | D: 0  |
	| location      | VARCHAR(255) | NTNUL |
    | latitude      | DECIMAL(10,8)|       |
    | longitude     | DECIMAL(11,8)|       |
    | country       | VARCHAR(255) | NTNUL |
    | started       | TIMESTAMP    | NTNUL |
    | ends          | TIMESTAMP    | NTNUL |
    | seller        | VARCHAR(255) | FOR   |
    | description   | VARCHAR(4000)| D: "" |
    +---------------+--------------+-------+

    Sellers
    +---------------+--------------+-------+
    | FIELD         | TYPE         | ATTR  |
    +---------------+--------------+-------+
    | userId        | VARCHAR(255) | PRI   |
    | rating        | INTEGER      | D: 0  |
    +---------------+--------------+-------+
    
	Bidders
	+---------------+--------------+-------+
    | FIELD         | TYPE         | ATTR  |
    +---------------+--------------+-------+
    | userId        | VARCHAR(255) | PRI   |
    | rating        | INTEGER      | D: 0  |
    | location      | VARCHAR(255) |       |
    | country       | VARCHAR(255) |       |
	+---------------+--------------+-------+

    Bids
    +---------------+--------------+-------+
    | FIELD         | TYPE         | ATTR  |
    +---------------+--------------+-------+
	| itemId        | INTEGER      | FOR   |
    | bidder        | VARCHAR(255) | FOR   |
    | time          | TIMESTAMP    | NTNUL |
    | amount        | DECIMAL(8,2) | D: 0  |
	+---------------+--------------+-------+
	
	Categories
    +---------------+--------------+-------+
    | FIELD         | TYPE         | ATTR  |
    +---------------+--------------+-------+
	| itemId        | INTEGER      | FOR   |
    | category      | VARCHAR(255) | NTNUL |
	+---------------+--------------+-------+