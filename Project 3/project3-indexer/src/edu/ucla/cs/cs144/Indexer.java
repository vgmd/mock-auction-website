package edu.ucla.cs.cs144;

import java.io.IOException;
import java.io.StringReader;
import java.io.File;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
// Timestamp imports
import java.sql.Timestamp;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Indexer {
    
    /** Creates a new instance of Indexer */
    public Indexer() {
    }
 
    public void rebuildIndexes() {

        Connection conn = null;

        // create a connection to the database to retrieve Items from MySQL
	try {
	    conn = DbManager.getConnection(true);
	} catch (SQLException ex) {
	    System.out.println(ex);
	}


	/*
	 * Add your code here to retrieve Items using the connection
	 * and add corresponding entries to your Lucene inverted indexes.
         *
         * You will have to use JDBC API to retrieve MySQL data from Java.
         * Read our tutorial on JDBC if you do not know how to use JDBC.
         *
         * You will also have to use Lucene IndexWriter and Document
         * classes to create an index and populate it with Items data.
         * Read our tutorial on Lucene as well if you don't know how.
         *
         * As part of this development, you may want to add 
         * new methods and create additional Java classes. 
         * If you create new classes, make sure that
         * the classes become part of "edu.ucla.cs.cs144" package
         * and place your class source files at src/edu/ucla/cs/cs144/.
	 * 
	 */
    
    try {
        Directory indexDir = FSDirectory.open(new File("/var/lib/lucene/index"));
        IndexWriterConfig config =
            new IndexWriterConfig(Version.LUCENE_4_10_2, new StandardAnalyzer());
        IndexWriter indexWriter = new IndexWriter(indexDir, config);
        
        Statement stmt = conn.createStatement();

        ResultSet rs = stmt.executeQuery("SELECT * FROM Items");
        while (rs.next()) {
            Document doc = new Document();
            doc.add(new StringField("itemId",
                Integer.toString(rs.getInt("itemId")), Field.Store.YES));
            doc.add(new TextField("name", rs.getString("name"), Field.Store.YES));
            
            doc.add(new StringField("currently",
                Double.toString(rs.getDouble("currently")), Field.Store.NO));
            doc.add(new StringField("buyPrice",
                Double.toString(rs.getDouble("buyPrice")), Field.Store.NO));
            doc.add(new StringField("firstBid",
                Integer.toString(rs.getInt("firstBid")), Field.Store.NO));
            doc.add(new StringField("numBids",
                Integer.toString(rs.getInt("numBids")), Field.Store.NO));
            doc.add(new TextField("location", rs.getString("location"), Field.Store.NO));
            doc.add(new StringField("latitude",
                Double.toString(rs.getDouble("latitude")), Field.Store.NO));
            doc.add(new StringField("longitude",
                Double.toString(rs.getDouble("longitude")), Field.Store.NO));
            doc.add(new TextField("country", rs.getString("country"), Field.Store.NO));
            
            // Timestamps
            Timestamp started = rs.getTimestamp("started");
            doc.add(new StringField("started", started.toString(), Field.Store.NO));
            Timestamp ends = rs.getTimestamp("ends");
            doc.add(new StringField("ends", ends.toString(), Field.Store.NO));
            
            doc.add(new StringField("seller", rs.getString("seller"), Field.Store.NO));
            
            doc.add(new TextField("description", rs.getString("description"), Field.Store.NO));
            
            // This is used for union of category, name, and category
            String totalText = rs.getString("description") + " " + rs.getString("name");

            // Get categories
            Statement catStmt = conn.createStatement();
            ResultSet categories = catStmt.executeQuery("SELECT * FROM Categories WHERE itemId = "
                + rs.getInt("itemId"));

            while (categories.next()) {
                totalText += " " + categories.getString("category");
            }
            
            doc.add(new TextField("totalText", totalText, Field.Store.NO));
            indexWriter.addDocument(doc);
        }
        
        indexWriter.close();
    } catch (SQLException ex) {
        System.err.println("SQLException: " + ex.getMessage()); 
    } catch (IOException e) {
        System.err.println("IOException: " + e.getMessage()); 
    }
    

        // close the database connection
	try {
	    conn.close();
	} catch (SQLException ex) {
	    System.out.println(ex);
	}
    }    

    public static void main(String args[]) {
        Indexer idx = new Indexer();
        idx.rebuildIndexes();
    }   
}
