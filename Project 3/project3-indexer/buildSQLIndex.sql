CREATE TABLE SpatialIndex(
    itemId int,
    coordinates GEOMETRY NOT NULL
) ENGINE=MyISAM;

-- Create indices
ALTER TABLE SpatialIndex ADD PRIMARY KEY (itemId);
ALTER TABLE SpatialIndex ADD SPATIAL INDEX(coordinates);

-- Load information into table
INSERT INTO SpatialIndex(itemId, coordinates)
SELECT itemId, Point(latitude, longitude)
FROM Items;